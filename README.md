# Mise à niveau math pour les apprentis

## Contenu
- `Cours-TD/slides_chap1.pdf`: chapitre 1 du cours
- `Cours-TD/td1.pdf`: td1
- `Complements/Chap_suites_fonctions__utf8 copie.pdf` : chapitre très complet sur les suites, fonctions et Développements limités 

Pour récupérer les sources, il faut cloner ce dépot git. Faire la commande suivante dans un terminal :

```bash
git clone https://gitlab.irit.fr/toc/mathn7/mise-a-niveau-apprentis/etudiants.git
```

Vous pouvez ensuite renommer le répertoire parent. Une autre possibilité est de le faire directement par le clone :

```bash
git clone repo-name folder-name
```
